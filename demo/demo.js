import '@casino/demo-css/';
import createElementFromHTML from '../';

let element = createElementFromHTML(
    `<div>
        <h1>Title</h1>
        <p>Text</p>
    </div>`
);
document.getElementById('js').appendChild(element);