# README

## Installation
```bash
cd /path/to/project
yarn install
```

## Build demo
```bash
yarn webpack
```

## Use
```js
import createElementFromHTML from '@casino/create-element-from-html';

let element = createElementFromHTML(
    `<div>
        <h1>Title</h1>
        <p>Text</p>
    </div>`
);
document.getElementById('js').appendChild(element);
```

## Limits
Parent tag must be only single. **VALID:**
```html
<div>
    <h1>Title</h1>
    <p>Text</p>
</div>
```

**INVALID:**
```html
<h1>Title</h1>
<p>Text</p>
```